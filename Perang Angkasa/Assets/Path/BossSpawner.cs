﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    [SerializeField] int startingWave = 0;
    [SerializeField] private bool isLooping = false;
    [SerializeField] List<WaveConfig> bossWaveConfigs;
    
    public IEnumerator SpawnBossWaves()
    {
        for (int waveIndex = startingWave; waveIndex < bossWaveConfigs.Count; waveIndex++)
        {
            var currentWave = bossWaveConfigs[waveIndex];
            yield return StartCoroutine(SpawnAllBossInWave(currentWave));
        }
    }
    public IEnumerator SpawnAllBossInWave(WaveConfig bosswaveConfig)
    {
        for (int enemyCount = 0; enemyCount < bosswaveConfig.GetNumberOfEnemies(); enemyCount++)
        {
            var newEnemy = Instantiate(
                bosswaveConfig.GetEnemyPrefab(),
                bosswaveConfig.GetWaypoints()[0].transform.position,
                Quaternion.identity);
            newEnemy.GetComponent<BossPath>().SetWaveConfig(bosswaveConfig);
            yield return new WaitForSeconds(bosswaveConfig.GetTimeBetweenSpawns());
        }
    }

}
