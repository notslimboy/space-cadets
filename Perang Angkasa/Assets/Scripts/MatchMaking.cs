﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MatchMaking : MonoBehaviour
{
  [SerializeField] private int health;
  private PlayerMovement _playerMovement;
  private BossSpawner _bossSpawner;
  private bool isStop = false;
  
  private void Start()
  {
    SettingUpComp();
  }

  private void Update()
  {
    if (!isStop)
    {
      ChekingParameter();
    }
    
  }

  void SettingUpComp()
  {
    _playerMovement = GameObject.FindObjectOfType<PlayerMovement>();
    _bossSpawner = GameObject.FindObjectOfType<BossSpawner>();
  }

  void ChekingParameter()
  {
    health = _playerMovement.health;
    if (health <= 500)
    {
      if (!isStop)
      {
        isStop = true;
        StartSpawn();
      }
    }
  }

  private void StartSpawn()
  {
    StartCoroutine(_bossSpawner.SpawnBossWaves());
  }
}
