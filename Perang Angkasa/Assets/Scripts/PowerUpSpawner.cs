using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;


public class PowerUpSpawner : MonoBehaviour
{
    
    [SerializeField] private bool isLooping = true;
    [SerializeField] private PowerUpConfig[] _powerUpConfig;
    private int randValue;

    IEnumerator Start()
    {
        do
        {
            randValue = Random.Range(0, _powerUpConfig.Length);
            yield return StartCoroutine(_powerUpConfig[randValue].SpawnPowerUps());
        } while (isLooping);
        
    }
}
