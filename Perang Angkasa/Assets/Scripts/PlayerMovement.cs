﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerMovement : MonoBehaviour
{
    [Header("Status")]
    [SerializeField] public float moveSpeed = 10f;
    [SerializeField] float padding = 1f;
    [SerializeField] public int health = 300;
    [SerializeField] private float delayBeforeDeath = 2f;
    private bool isDisable = false;


    [Header("Laser")]
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private float projectileSpeed;
    [SerializeField] public float projectileFiringPeriod = 0.1f;
    
    [Header("Player Vfx")] 
    [SerializeField] private GameObject deathVfx;
    [SerializeField] private float deathVfxDuration = 2f;
    [SerializeField] private GameObject GameOverPanel;

    Coroutine firingCoroutine;
    [SerializeField] Text playerHealthText;
    
    float xMin;
    float xMax;
    float yMin;
    float yMax;

    private int healthAdder;
    
    //update value
    private float defaultSpeed;
    private float defaultFireSpeed;
    //timeUpdatePowerUps;
    [SerializeField] private float timerPowerUps;
    private float defaultTimerPowerUps;

    // Use this for initialization
    void Start ()
    {
        SetUpMoveBoundaries();

        //startSpeed
        defaultSpeed = moveSpeed;
        defaultFireSpeed = projectileFiringPeriod;
    }
    
    void Update () 
    {
        if (!isDisable)
        {
            Move();
            Fire();
        }
        PlayerHealth();

        //timerPowerUps
        if (moveSpeed > defaultSpeed || projectileSpeed > projectileFiringPeriod) {
            defaultTimerPowerUps += 1 * Time.deltaTime;
            if (defaultTimerPowerUps >= timerPowerUps)
            {
                moveSpeed = defaultSpeed;
                projectileFiringPeriod = defaultFireSpeed;
                defaultTimerPowerUps = 0;
            }
        }
    }


    
    private void PlayerHealth()
    {
        playerHealthText.text = "Health : " + health.ToString();
    }
    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            firingCoroutine = StartCoroutine(FireContinuously());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCoroutine);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if(!damageDealer) {return;}
        ProccesHit(damageDealer);
    }
    
    private void ProccesHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
        {
            Death();
        }
    }
    private void Death()
    {
            LeanTween.scale(GameOverPanel.GetComponent<RectTransform>(), new Vector3(1f,1f,1f), 0.5f).setEaseInOutCubic();
            isDisable = true;
            health = 0;
            PlayerHealth();
            GameObject explosion = Instantiate(deathVfx, transform.position, transform.rotation);
            Destroy(explosion, deathVfxDuration);
            Destroy(gameObject);
           
    }
    

    IEnumerator FireContinuously()
    {
        while (true)
        {
            GameObject laser = Instantiate(
                laserPrefab,
                transform.position,
                Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
            yield return new WaitForSeconds(projectileFiringPeriod);
        }
    }

    
    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
        transform.position = new Vector2(newXPos, newYPos);
    }
    
    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }

}