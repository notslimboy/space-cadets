﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "PowerUp Config")]
public class PowerUpConfig : ScriptableObject
{
    [SerializeField] private GameObject _powerUpPrefabs;
    [SerializeField] float minTimeBetweenSpawns = 0.5f;
    [SerializeField] float maximeBetweenSpawns = 10f;
    [SerializeField] public float firingPeriod;
    [SerializeField] public float moveSpeed;
    [SerializeField] public int health;
     private float minXSpawn = 5.6f;
     private float maxXSpawn = 5.6f;
    [SerializeField] private float powerSpawnSpeed;

    public GameObject GetPowerUpPrefabs()
    {
        return _powerUpPrefabs;
    }

    public float GetFiringPeriod()
    {
        return firingPeriod;
    }

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public int GetHealth()
    {
        return health;
    }

    public float GetTimeBetweenSpawns()
    {
        var powerTimeRandom = Random.Range(minTimeBetweenSpawns, maximeBetweenSpawns);
        return powerTimeRandom;
    }
    
   public IEnumerator SpawnPowerUps()
   {
       // Position
        var yPos = 10;
        var RandomXPosition = new Vector2(Random.Range(-minXSpawn, maxXSpawn), yPos );
        
        // Spawn
        var newPowerUps = Instantiate(
            GetPowerUpPrefabs(),RandomXPosition
            , Quaternion.identity);
        
        // Attach Component
        newPowerUps.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -powerSpawnSpeed);

        // loop between random time 
        yield return  new WaitForSeconds(GetTimeBetweenSpawns());
    }
}
