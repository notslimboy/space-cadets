﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [Header("Status")]
    [SerializeField] float health = 100;
    [SerializeField] private int score;
    
    [Header("Shot Period")]
    [SerializeField] private float shotCounter;
    [SerializeField] private float minTimeBetweenShots;
    [SerializeField] private float maxTimeBetweenShots;
    
    [Header("Projectile")]
    [SerializeField] private GameObject projectile;
    [SerializeField] private float projectileSpeed;
    
    [Header("Enemy Vfx")] 
    [SerializeField] private GameObject deathVfx;
    [SerializeField] private float deathVfxDuration = 1.5f;


    private void Start()
    {
        InitiatShotsTimes();
    }

    private void Update()
    {
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0 )
        {
            Fire();
            InitiatShotsTimes();
        }
    }

    private void Fire()
    {
        GameObject laser = Instantiate(
            projectile,
            transform.position,
            Quaternion.identity
        ) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
    }

    private void InitiatShotsTimes()
    {
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if(!damageDealer) {return;}
        ProccesHit(damageDealer);
    }
    private void ProccesHit(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
        {
            Death();
        }
    }

    private void Death()
    {
        FindObjectOfType<Score>().AddToScore(score);
        Destroy(gameObject);
        GameObject explosion = Instantiate(deathVfx, transform.position, transform.rotation);
        Destroy(explosion, deathVfxDuration);
    }
}
