﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveCanvas : MonoBehaviour
{
    [SerializeField] private GameObject _settingPanel, _creditPanel,mainPanel,exitPanel;

    public void CreditOn()
    {
        LeanTween.moveX(_creditPanel.GetComponent<RectTransform>(), 0, 0.5f).setEaseInCubic();
        LeanTween.moveX(mainPanel.GetComponent<RectTransform>(), -1000, 0.6f).setEaseOutCubic();
    }

    public void CreditOff()
    {
        LeanTween.moveX(_creditPanel.GetComponent<RectTransform>(), 1000, 0.6f);
        LeanTween.moveX(mainPanel.GetComponent<RectTransform>(), 0, 0.5f);
    }

    public void SettingOn()
    {
        LeanTween.moveX(mainPanel.GetComponent<RectTransform>(), 1000, 0.6f).setEaseOutCubic();
        LeanTween.moveX(_settingPanel.GetComponent<RectTransform>(), 0, 0.6f).setEaseInCubic();
    }

    public void SettingOff()
    {
        LeanTween.moveX(mainPanel.GetComponent<RectTransform>(), 0, 0.5f).setEaseInCubic();
        LeanTween.moveX(_settingPanel.GetComponent<RectTransform>(), -1000, 0.6f).setEaseInCubic();
    }

    public void ExitedOnYou()
    {
        LeanTween.scale(exitPanel.GetComponent<RectTransform>(), new Vector3(1f,1f,1f), 0.5f).setEaseInOutCubic();
    }

    public void ExitedOffYou()
    {
        LeanTween.scale(exitPanel.GetComponent<RectTransform>(), new Vector3(0f,0f,0f), 0.5f).setEaseOutCubic();
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
