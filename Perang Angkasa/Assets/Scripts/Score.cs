﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;
    int score = 0;
    private void Awake()
    {
        SetUpSingleton();
    }

    private void Update()
    {
        scoreText.text = score.ToString();
    }

    private void SetupScoreText()
    {
        scoreText = GetComponent<Text>();
    }

    private void SetUpSingleton()
    {
        int numberGameSessions = FindObjectsOfType<Score>().Length;
        if (numberGameSessions > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public int GetScore()
    {
        return score;
    }

    public void AddToScore(int scoreValue)
    {
        score += scoreValue;
    }
    
}
