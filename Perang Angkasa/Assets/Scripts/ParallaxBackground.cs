﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] private float scrollSpeed;
    private Material background;
    private Vector2 offSet;
    void Start()
    {
        background = GetComponent<Renderer>().material;
        offSet = new Vector2(0f, scrollSpeed);
    }

    
    void Update()
    {
        background.mainTextureOffset += offSet * Time.deltaTime;
    }
}
