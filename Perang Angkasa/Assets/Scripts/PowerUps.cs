using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class PowerUps : MonoBehaviour
{
    
    [SerializeField]private PowerUpConfig _powerUpConfig;
    [SerializeField] private PlayerMovement _playerMovement;
    [SerializeField] private GameObject popVfx;
    [SerializeField] private int currentPlayerHealth;
    [SerializeField] private float currentPlayerFiringPeriod;
    [SerializeField] private float currentPlayerMoveSpeed;
    [SerializeField] private int PlayerHealthAdder;
    [SerializeField] private float PlayerFiringPeriodAdder;
    [SerializeField] private float PlayerMoveSpeedAdder;
    
    
    

    private void Start()
    {
        if (GameObject.FindObjectOfType<PlayerMovement>() != null)
        {
            _playerMovement = GameObject.FindObjectOfType<PlayerMovement>();
            currentPlayerHealth = _playerMovement.health;
        }

        PlayerHealthAdder = _powerUpConfig.GetHealth();
        Debug.Log(PlayerHealthAdder);
    }

    private void Update()
    {
        if (_playerMovement != null) {
            currentPlayerHealth = _playerMovement.health;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag != "Player") return;
        Debug.Log("Hit");
        Hit();
        HealthAdder();
        FireSpeedAdder();
        MoveSpeedAdder();
    }

    public void Hit()
    {
        GameObject powerUps = Instantiate(popVfx, transform.position, transform.rotation);
        Destroy(gameObject);
        Destroy(powerUps,1f);
    }

    public void HealthAdder()
    { 
        _playerMovement.health += PlayerHealthAdder;
    }

    public void FireSpeedAdder()
    {
        _playerMovement.projectileFiringPeriod += _powerUpConfig.firingPeriod;
    }

    public void MoveSpeedAdder()
    {
        _playerMovement.moveSpeed += _powerUpConfig.moveSpeed;
    }
}