﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance = null;
    public AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            return;
        }

        if (instance == this)
        {
            return;
        }

        Destroy(this.gameObject);
    }

    public void MusicON()
    {
        audio.Play(); 
    }

    public void MusicOff()
    {
        audio.Pause();
    }
}
